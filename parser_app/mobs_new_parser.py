import time
import requests
import json
from bs4 import BeautifulSoup as BS
from datetime import datetime
import os

start_time = time.time()


# Get information and creation directory
def info_url_units():
    url_name = "https://hommbase.ru/mobs"
    name_for_dir = 'data/' + url_name.replace("//", '.').replace("/", '.').split('.')[3]
    if not os.path.isdir(name_for_dir):
        os.mkdir(name_for_dir)
    name_for_dir = url_name.replace("//", '.').replace("/", '.').split('.')[3]
    domen_url = url_name.split('/' + name_for_dir)[0]
    return url_name, name_for_dir, domen_url


def get_data_units(url_name, name_for_dir, domen_url):
    cur_time = datetime.now().strftime("%d_%m_%Y")
    #
    # headers = {
    #     "accept": "*/*",
    #     "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
    # }
    #
    # url = "https://hommbase.ru/mobs"
    #
    # req = requests.get(url, headers=headers)
    # src = req.text
    #
    # with open(f"data/{name_for_dir}/{name_for_dir}_{cur_time}.html", "w", encoding="utf-8") as file:
    #     file.write(src)

    # Вопрос к открытию файла при смене дня!!!
    with open(f"data/{name_for_dir}/{name_for_dir}_{cur_time}.html", encoding="utf-8") as file:
        src = file.read()

    soup = BS(src, "lxml")
    body = soup.find_all("div", class_="mob object-card")
    count = 0
    list_unit = []
    for each in body:
        count += 1
        # Parsing information about a unit
        engname = each.get('data-engname', "None").title()
        rusname = each.get('data-rusname', "None").title()
        attack = each.get('data-attack', "None")
        averagedamage = each.get('data-averagedamage', "None")
        castle = each.get('data-castle', "None").capitalize()
        cost = each.get('data-cost', "None").replace('_', ' ')
        damage = each.get('data-damage', "None")
        defense = each.get('data-defense', "None")
        growth = each.get('data-growth', "None")
        health = each.get('data-growth', "None")
        land = each.get('data-land', "None")
        level = each.get('data-level', "None")
        move = each.get('data-move', "None")
        number = each.get('data-number', "None")
        shoots = each.get('data-shoots', "None")
        speed = each.get('data-speed', "None")
        stat = each.get('data-stat', "None")
        up = each.get('data-up', "None")
        AI_value = each.get('data-value', "None")
        img = domen_url + each.find('img').get("data-src", "None")
        ability = each.get('data-ability', "None").split('.')
        list_ability = []
        for i in ability:
            if '<a' not in i and 'Подробнее' not in i:
                list_ability.append(i)
        list_ability = '.'.join(list_ability)
        # Collecting all information about the unit
        list_unit.append(
            {
                'engname': engname,
                'rusname': rusname,
                'attack': attack,
                'averagedamage': averagedamage,
                'castle': castle,
                'cost': cost,
                'damage': damage,
                'defense': defense,
                'growth': growth,
                'health': health,
                'land': land,
                'level': level,
                'move': move,
                'number': number,
                'shoots': shoots,
                'speed': speed,
                'stat': stat,
                'up': up,
                'AI_value': AI_value,
                'img': img,
                'list_ability': list_ability

            }
        )
        print(f"Обработан {engname} юнит ...")



    with open(f"data/{name_for_dir}/{name_for_dir}_{cur_time}.json", "w", encoding="utf-8") as file:
        json.dump(list_unit, file, indent=4, ensure_ascii=False)
    print("#"*100)
    print(f"WellDone!!!. Было сохранено {count} персонажей из Heroes of Might and Magic III. Все данные сохранены в {name_for_dir}_{cur_time}.json")

def main():
    info = info_url_units()
    get_data_units(info[0], info[1], info[2])
    full_time = time.time() - start_time
    print(f"На работу скрипта потрачено: {full_time} сек.")


if __name__ == "__main__":
    main()

import time
import requests
import json
from bs4 import BeautifulSoup as BS
from datetime import datetime
import os

start_time = time.time()


# Get information and creation directory
def info_url_artifact():
    url_name = "https://hommbase.ru/artifacts"

    name_for_dir = 'data/' + url_name.replace("//", '.').replace("/", '.').split('.')[3]
    if not os.path.isdir(name_for_dir):
        os.mkdir(name_for_dir)
    name_for_dir = url_name.replace("//", '.').replace("/", '.').split('.')[3]
    domen_url = url_name.split('/' + name_for_dir)[0]
    return url_name, name_for_dir, domen_url


def get_data_artifact(url_name, name_for_dir, domen_url):
    cur_time = datetime.now().strftime("%d_%m_%Y")
    # headers = {
    #     "accept": "*/*",
    #     "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
    # }
    #
    # url = url_name
    #
    # req = requests.get(url, headers=headers)
    # src = req.text
    #
    # with open(f"data/{name_for_dir}/{name_for_dir}_{cur_time}.html", "w", encoding="utf-8") as file:
    #     file.write(src)

    # Вопрос к открытию файла при смене дня!!!
    with open(f"data/{name_for_dir}/{name_for_dir}_{cur_time}.html", encoding="utf-8") as file:
        src = file.read()

    soup = BS(src, "lxml")
    body = soup.find_all("div", class_="artifact object-card")
    count = 0
    list_artifact = []
    for each in body:
        count += 1
        # Parsing information about a artifact
        engname = each.get('data-engname', "None").title()
        rusname = each.get('data-rusname', "None").title()
        category = each.get('data-category', "None")
        id_artifact = each.get('data-id', "None")
        cost = each.get('data-cost', "None").replace('_', ' ').strip()
        sale = each.get('data-sale', "None").replace('_', ' ').strip()
        picture = each.get('data-picture', "None")
        slot = each.get('data-slot', "None")
        value = each.get('data-value', "None")
        set = each.get('data-set').replace('<br>', '').replace('<mark class=one>', '').replace('<b>', '').replace(
            '</b>', '').replace('</mark>', '').replace(']', '').replace('[', 'Собрав полный комплект вы получаете: ')
        if len(set) < 1:
            set = "None"
        level = each.get('data-level', "None")
        img = domen_url + each.find('img').get("data-src", "None")
        bonus = each.get('data-bonus', "None").split('.')
        list_bonus = []
        for i in bonus:
            if 'Подробнее' not in i:
                list_bonus.append(i)
        list_bonus = '.'.join(list_bonus)

        # Collecting all information about the artifact
        list_artifact.append(
            {
                'engname': engname,
                'rusname': rusname,
                'category': category,
                'id_artifact': id_artifact,
                'cost': cost,
                'sale': sale,
                'picture': picture,
                'slot': slot,
                'value': value,
                'artifact_set': set,
                'level': level,
                'img': img,
                'bonus': list_bonus,
            }
        )

        print(f"Обработан {engname} артефакт ...")

    with open(f"data/{name_for_dir}/{name_for_dir}_{cur_time}.json", "w", encoding="utf-8") as file:
        json.dump(list_artifact, file, indent=4, ensure_ascii=False)
    print("#" * 100)
    print(
        f"WellDone!!!. Было сохранено {count} Артефактов из Heroes of Might and Magic III. Все данные сохранены в {name_for_dir}_{cur_time}.json")


def main():
    info = info_url_artifact()
    get_data_artifact(info[0], info[1], info[2])
    full_time = time.time() - start_time
    print(f"На работу скрипта потрачено: {full_time} сек.")


if __name__ == "__main__":
    main()

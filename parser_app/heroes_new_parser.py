import time
import requests
import json
from bs4 import BeautifulSoup as BS
from datetime import datetime
import os

start_time = time.time()


# Get information and creation directory
def info_url_heroes():
    url_name = "https://hommbase.ru/heroes"
    name_for_dir = 'data/' + url_name.replace("//", '.').replace("/", '.').split('.')[3]
    if not os.path.isdir(name_for_dir):
        os.mkdir(name_for_dir)
    name_for_dir = url_name.replace("//", '.').replace("/", '.').split('.')[3]
    domen_url = url_name.split('/' + name_for_dir)[0]
    return url_name, name_for_dir, domen_url


def get_data_heroes(url_name, name_for_dir, domen_url):
    cur_time = datetime.now().strftime("%d_%m_%Y")
    headers = {
        "accept": "*/*",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
    }

    url = url_name

    req = requests.get(url, headers=headers)
    src = req.text

    with open(f"data/{name_for_dir}/{name_for_dir}_{cur_time}.html", "w", encoding="utf-8") as file:
        file.write(src)

    # Вопрос к открытию файла при смене дня!!!
    with open(f"data/{name_for_dir}/{name_for_dir}_{cur_time}.html", encoding="utf-8") as file:
        src = file.read()

    soup = BS(src, "lxml")
    body = soup.find_all("div", class_="hero object-card")
    count = 0
    list_heroes = []
    for each in body:
        count += 1
        # Parsing information about a heroes
        engname = each.get('data-engname', "None").capitalize()
        rusname = each.get('data-rusname', "None").capitalize()
        bio = each.get('data-bio', "None")
        heroes_rank = each.get('data-rank', "None")
        castle = each.get('data-castle', "None").capitalize()
        primary = each.get('data-primary', "None").split('/')
        race = each.get('data-race', "None")
        secondary1 = each.get('data-secondary1', "None")
        secondary2 = each.get('data-secondary2', "None")
        sex = each.get('data-sex', "None")
        specials = each.get('data-spec', "None").replace('<mark class=one>', '').replace('</mark>', '').split(".")
        spell = each.get('data-spell', "None")
        img = domen_url + each.find('img').get("data-src", "None")
        list_primaty = []
        if primary:
            list_primaty = ['Атака: ' + primary[0], 'Защита: ' + primary[1], 'Сила магии: ' + primary[2],
                            'Знания: ' + primary[3]]
        list_primaty = ' / '.join(list_primaty)
        list_specials = []
        for i in specials:
            if "Подробнее" not in i:
                list_specials.append(i)
        list_specials = '.'.join(list_specials)
        list_secondary = []
        if len(secondary2) < 1:
            list_secondary = secondary1.capitalize()
        else:
            list_secondary = secondary1.capitalize() + ' / ' + secondary2.capitalize()

        # Collecting all information about the heroes
        list_heroes.append(
            {
                'engname': engname,
                'rusname': rusname,
                'castle': castle,
                'sex': sex,
                'heroes_rank': heroes_rank,
                'race': race,
                'bio': bio,
                'primary_heroes': list_primaty,
                'specials': list_specials,
                'spell': spell,
                'img': img,
                'list_secondary': list_secondary,
            }
        )

        print(f"Обработан Герой {engname} ...")

    with open(f"data/{name_for_dir}/{name_for_dir}_{cur_time}.json", "w", encoding="utf-8") as file:
        json.dump(list_heroes, file, indent=4, ensure_ascii=False)
    print("#" * 100)
    print(
        f"WellDone!!!. Было сохранено {count} Героев из Heroes of Might and Magic III. Все данные сохранены в {name_for_dir}_{cur_time}.json")


def main():
    info = info_url_heroes()
    get_data_heroes(info[0], info[1], info[2])
    full_time = time.time() - start_time
    print(f"На работу скрипта потрачено: {full_time} сек.")


if __name__ == "__main__":
    main()

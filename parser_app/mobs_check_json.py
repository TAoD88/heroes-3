import json
from mobs_new_parser import info_url_units
import os

list_n = []
dir_name = info_url_units()[1]

path = f"data/{dir_name}"
# Sort file names with path we need last
file_list = os.listdir(path)
full_list = [os.path.join(path, i) for i in file_list]
for list_name in full_list:
    if list_name.endswith("json"):
        list_n.append(list_name)
time_sorted_list = sorted(list_n, key=os.path.getmtime)
file = time_sorted_list[-1].split('/')[-1]

# Open file
with open(f"data/{file}", encoding="utf-8") as file:
    json_data = file.read()


# Get headers for DB
# Get information from card for DB (num)
def tab_t_str(num):
    json_odj = json.loads(json_data)
    column = ', '.join(str(x) for x in json_odj[0].keys())
    data = ', '.join("'" + str(x) + "'" for x in json_odj[num].values())
    all_title_names = []
    for key in json_odj[0].keys():
        all_title_names.append(key)
    # print(column, "-----", data, "-----", all_title_names)
    return column, data, all_title_names


# Get amount from table_information if you need
def len_json_file():
    len_json = len(json.loads(json_data))
    # print(len_json)
    return len_json

def main():
    len_json_file()
    tab_t_str(0)

if __name__ == "__main__":
    main()

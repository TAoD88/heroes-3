import mysql
from mysql.connector import Error
from datetime import datetime
import time
from server import db_conf
from spell_check_json import tab_t_str, len_json_file
from spell_new_parser import info_url_spells

table_name = info_url_spells()[1].capitalize()
start_time = time.time()
cur_time = datetime.now().strftime("%d_%m_%Y")


# Get connection and check it
def create_connectoin_db(user_name, user_password, host_name, db_name=None):
    connectoin_db = None
    try:
        connectoin_db = mysql.connector.connect(
            user=user_name,
            password=user_password,
            host=host_name,
            database=db_name
        )
        print("База данных успешно подключена!")
    except Error as db_connection_error:
        print("Ошибка подключения :", db_connection_error)
    return connectoin_db


# Check DB name, if not in, create DB name
def check_db(conn):
    cursor = conn.cursor()
    try:
        cursor.execute(
            "CREATE DATABASE IF NOT EXISTS {} ".format(db_conf["sql"]["db_name"]))
    except mysql.connector.Error as err:
        print("Failed creating database: {}".format(err))
        exit(1)
    cursor.close()
    conn.close()
    print("База данных проверена")


# Check table name, if not in, create table name
def check_table_db(conn2):
    cursor = conn2.cursor()
    table = tab_t_str(0)[2]
    create_table_title = '''
    CREATE TABLE IF NOT EXISTS {} (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (id) );'''.format(
        table_name)  # тут с помощью формата можно создавать имя таблице
    cursor.execute(create_table_title)
    conn2.commit()

    # Check columns in table
    for col_name in table:
        column_name = "ALTER TABLE {} ADD COLUMN {} LONGTEXT NOT NULL;".format(table_name, col_name)
        try:
            cursor.execute(column_name)
            conn2.commit()
        except:
            conn2.rollback()

    # Enter the values in table
    count = 0
    for i in range(0, len_json_file()):
        count += 1
        data = '''INSERT INTO %s (%s) VALUES (%s);''' % (table_name, tab_t_str(i)[0], tab_t_str(i)[1])
        try:
            cursor.execute(data)
            conn2.commit()
            print(f' Good: {count}')
        except:
            conn2.rollback()
            print(f' Bed: {count}')

    # cursor.close()
    # conn2.close()

    print(f"База была обновлена до версии {cur_time}")

def main():
    conn = create_connectoin_db(db_conf["sql"]["user"],
                                db_conf["sql"]["pass"],
                                db_conf["sql"]["host"])
    check_db(conn)

    conn2 = create_connectoin_db(db_conf["sql"]["user"],
                                 db_conf["sql"]["pass"],
                                 db_conf["sql"]["host"],
                                 db_conf["sql"]["db_name"])
    check_table_db(conn2)

    full_time = time.time() - start_time
    print(f"На работу скрипта потрачено: {full_time} сек.")


if __name__ == "__main__":
    main()

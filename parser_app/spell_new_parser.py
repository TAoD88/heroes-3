import time
import requests
import json
from bs4 import BeautifulSoup as BS
from datetime import datetime
import os

start_time = time.time()


# Get information and creation directory
def info_url_spells():
    url_name = "https://hommbase.ru/spells"
    name_for_dir = 'data/' + url_name.replace("//", '.').replace("/", '.').split('.')[3]
    if not os.path.isdir(name_for_dir):
        os.mkdir(name_for_dir)
    name_for_dir = url_name.replace("//", '.').replace("/", '.').split('.')[3]
    domen_url = url_name.split('/' + name_for_dir)[0]
    return url_name, name_for_dir, domen_url


def get_data_spells(url_name, name_for_dir, domen_url):
    cur_time = datetime.now().strftime("%d_%m_%Y")
    headers = {
        "accept": "*/*",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
    }

    url = url_name

    req = requests.get(url, headers=headers)
    src = req.text

    with open(f"data/{name_for_dir}/{name_for_dir}_{cur_time}.html", "w", encoding="utf-8") as file:
        file.write(src)

    # Вопрос к открытию файла при смене дня!!!
    with open(f"data/{name_for_dir}/{name_for_dir}_{cur_time}.html", encoding="utf-8") as file:
        src = file.read()

    soup = BS(src, "lxml")
    body = soup.find_all("div", class_="spell object-card")
    count = 0
    list_spells = []
    for each in body:
        count += 1
        # Parsing information about a Spell
        engname = each.get('data-engname', "None").capitalize()
        rusname = each.get('data-rusname', "None").capitalize()
        basic = each.get('data-basic', "None").replace('<b>', '').replace('</b>', '').split("<a")
        advance = each.get('data-advance', "None").replace('<b>', '').replace('</b>', '')
        expert = each.get('data-expert', "None").replace('<b>', '').replace('</b>', '')
        class_spell = each.get('data-class', "None")
        element = each.get('data-element', "None")
        level = each.get('data-level', "None")
        value = each.get('data-value', "None").split('/')
        img = domen_url + each.find('img').get("data-src", "None")

        list_value = []
        if value:
            list_value = ['Basic: ' + value[0], 'Advance: ' + value[1], 'Expert: ' + value[2]]
        list_value = ' / '.join(list_value)
        list_basic = []
        for i in basic:
            if "Подробнее" not in i:
                list_basic.append(i)
        list_basic = '.'.join(list_basic)

        # Collecting all information about the spell
        list_spells.append(
            {
                'engname': engname,
                'rusname': rusname,
                'basic': list_basic,
                'advance': advance,
                'expert': expert,
                'class_spell': class_spell,
                'element': element,
                'level': level,
                'value': list_value,
                'img': img
            }
        )

        print(f"Обработано Заклинание: {engname} ...")

    with open(f"data/{name_for_dir}/{name_for_dir}_{cur_time}.json", "w", encoding="utf-8") as file:
        json.dump(list_spells, file, indent=4, ensure_ascii=False)
    print("#" * 100)
    print(
        f"WellDone!!!. Было сохранено {count} Заклинаний из Heroes of Might and Magic III. Все данные сохранены в {name_for_dir}_{cur_time}.json")


def main():
    info = info_url_spells()
    get_data_spells(info[0], info[1], info[2])
    full_time = time.time() - start_time
    print(f"На работу скрипта потрачено: {full_time} сек.")


if __name__ == "__main__":
    main()

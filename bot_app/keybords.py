from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton

menu_1 = ['🥷🏻 Mobs', '🤴🏻 Heroes', '📚 Spells', '🏺 Artifacts']
menu_2 = ['🙏🏻 Help', '💱 Donations', '/start']


def key_bord_menu():
    kb_m_1 = ReplyKeyboardMarkup(resize_keyboard=True)
    kb_m_1.add(*[KeyboardButton(text=button) for button in menu_1])
    kb_m_1.add(*[KeyboardButton(text=button) for button in menu_2])
    return kb_m_1


# Клавиатура для обработки MOBS, ARTIFACTS, HEROES, SPELLS  ###########################################################
def inkey_bord_1(data):
    ikb_1 = InlineKeyboardMarkup(row_width=3)  # создаём клавиатуру Замков
    ikb_1.add(*[InlineKeyboardButton(button, callback_data=button) for button in data])
    ikb_1.add(InlineKeyboardButton('Cancel', callback_data='cancel'))
    return ikb_1


def inkey_bord_2(data):
    ikb_2 = InlineKeyboardMarkup(row_width=3)  # создаём клавиатуру Уровней
    ikb_2.add(*[InlineKeyboardButton(button, callback_data=button) for button in data],
              InlineKeyboardButton("🔙 Back", callback_data="come_back"))
    return ikb_2

#    ###################################################################################################

# Get slot
def slot(cursor):
    cursor.execute('''SELECT DISTINCT slot FROM artifacts''')
    slots = cursor.fetchall()
    # print([i[0] for i in slots])
    return [i[0] for i in slots]

# Get value
def value(cursor, slot='голова'):
    cursor.execute('''SELECT DISTINCT value FROM artifacts WHERE slot ='{}' '''.format(slot))
    values = cursor.fetchall()
    # print([i[0] for i in sex])
    return [i[0] for i in values]

# Get Artifacts
def list_artifacts(cursor, slot='голова', value='1'):
    cursor.execute('''SELECT * FROM artifacts WHERE slot ='{}' AND value='{}' '''.format(slot, value))
    artifacts = cursor.fetchall()
    list_artifact = []
    list_info_artifact = []
    for each in artifacts:
        list_artifact.append(each[1])
        list_info_artifact.append(each)
    return list_artifact, list_info_artifact

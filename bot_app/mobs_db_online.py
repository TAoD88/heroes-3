
# Get list_castle
def castle(cursor):
    cursor.execute('''SELECT DISTINCT castle FROM mobs''')
    castles = cursor.fetchall()
    return [i[0] for i in castles]

# Get levels
def level_mobs(cursor):
    cursor.execute('''SELECT DISTINCT level FROM mobs''')
    levels = cursor.fetchall()
    return [i[0] for i in levels if len(i[0]) == 1]


# Get units according
def list_unit_level(cursor, castle="Нет города", level='2'):
    cursor.execute('''SELECT * FROM mobs WHERE castle ='{}' AND level={} '''.format(castle, level))
    mobs = cursor.fetchall()
    list_unit = []
    list_info_unit = []
    for lev in mobs:
        list_unit.append(lev[1])
        list_info_unit.append(lev)
    return list_unit, list_info_unit

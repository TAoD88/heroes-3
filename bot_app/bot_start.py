from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from mysql.connector import Error
import mysql
from keybords import key_bord_menu, inkey_bord_1, inkey_bord_2

import logging
from config import db_config, TOKEN
from mobs_db_online import castle, level_mobs, list_unit_level
from heroes_db_online import castle_heroes, sex_heroes, rank_heroes, list_heroes
from spells_db_online import class_spell, element, list_spells
from artifacts_db_online import slot, value, list_artifacts
from all_text import DON, INFO_C, HELP_C, M_C, A_C, H_C, S_C, M_TITLE, H_TITLE, S_TITLE, A_TITLE

bot = Bot(TOKEN)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


# Создаем классы для STATE
class Mobs(StatesGroup):
    choose_castle = State()
    choose_level = State()
    choose_unit = State()


class Heroes(StatesGroup):
    choose_castle = State()
    choose_sex = State()
    choose_rank = State()
    choose_heroes = State()


class Spells(StatesGroup):
    choose_class = State()
    choose_element = State()
    choose_spells = State()


class Artifacts(StatesGroup):
    choose_slot = State()
    choose_value = State()
    choose_artifacts = State()


async def on_startup(_):
    logging.info('Bot is successfully launched!!!')


# Запускаем БД
def check_connectoin_db(user_name, user_password, host_name, db_name):
    connectoin_db = None
    try:
        connectoin_db = mysql.connector.connect(
            user=user_name,
            password=user_password,
            host=host_name,
            database=db_name
        )
        logging.info("База данных успешно подключена!")
    except Error as db_connection_error:
        logging.info("Ошибка подключения : {}".format(db_connection_error))
    return connectoin_db


conn = check_connectoin_db(db_config["user"],
                           db_config["pass"],
                           db_config["host"],
                           db_config["db_name"])
cursor = conn.cursor()


@dp.message_handler(commands=['start', '⭐️ start', '/start'])
async def start(message: types.Message):
    await bot.send_message(message.from_user.id,
                           text=INFO_C.format(message.from_user), parse_mode="HTML",
                           reply_markup=key_bord_menu())
    await message.delete()


@dp.message_handler(text=['Help', '🙏🏻 Help'])
async def help(message: types.Message):
    await bot.send_message(message.from_user.id,
                           text=HELP_C, reply_markup=key_bord_menu(),
                           parse_mode="HTML")
    await message.delete()


@dp.message_handler(text=['Donations', '💱 Donations'])
async def donat(message: types.Message):
    await bot.send_message(message.from_user.id,
                           text=DON.format(message.from_user), reply_markup=key_bord_menu(),
                           parse_mode="HTML")
    await message.delete()


####################### HANDLERS AND CALLBACKS FOR MOBS ###################################################

@dp.message_handler(text=['Mobs', '🥷🏻 Mobs'])
async def castle_unit(message: types.Message, state: FSMContext):
    list_data = castle(cursor)
    await bot.send_message(message.from_user.id,
                           text=M_TITLE[0],
                           parse_mode="HTML",
                           reply_markup=inkey_bord_1(list_data))
    await state.set_state(Mobs.choose_castle)


@dp.callback_query_handler(state=Mobs.choose_castle)
async def level_unit(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == "cancel":
        await state.finish()
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                message_id=callback_query.message.message_id,
            text=INFO_C.format(callback_query.message.from_user), parse_mode="HTML",
        )
        return

    await state.update_data(chosen_caslte=callback_query.data)
    list_data = level_mobs(cursor)
    user_data = await state.get_data()
    await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                message_id=callback_query.message.message_id,
                                reply_markup=inkey_bord_2(list_data),
                                text=M_TITLE[1].format(user_data['chosen_caslte']),
                                parse_mode="HTML")
    await state.set_state(Mobs.choose_level)


@dp.callback_query_handler(state=Mobs.choose_level)
async def list_unit(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'come_back':
        list_data = castle(cursor)
        user_data = await state.get_data()
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_1(list_data),
                                    text=M_TITLE[0],
                                    parse_mode="HTML")
        await state.set_state(Mobs.choose_castle)
    else:
        await state.update_data(chosen_level=callback_query.data)
        user_data = await state.get_data()
        upgrade_list = list_unit_level(cursor, castle=user_data['chosen_caslte'], level=user_data['chosen_level'])
        await state.update_data(all_units=upgrade_list[1])
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_2(upgrade_list[0]),
                                    text=M_TITLE[2].format(user_data['chosen_level'], user_data['chosen_caslte']),
                                    parse_mode="HTML")
        await state.set_state(Mobs.choose_unit)


@dp.callback_query_handler(state=Mobs.choose_unit)
async def show_unit(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'come_back':
        list_data = level_mobs(cursor)
        user_data = await state.get_data()
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_2(list_data),
                                    text=M_TITLE[1].format(user_data['chosen_caslte']),
                                    parse_mode="HTML")
        await state.set_state(Mobs.choose_level)
    else:
        user_data = await state.get_data()
        text_msg = 'none'
        for each in user_data['all_units']:
            if each[1] == callback_query.data:
                text_msg = M_C.format(each[1], each[2], each[5], each[12], each[17], each[13],
                                      each[3], each[8], each[7], each[15], each[10], each[16],
                                      each[9], each[19], each[6], each[21])
                await bot.send_photo(
                    chat_id=callback_query.message.chat.id,
                    photo=each[20],
                    caption=text_msg,
                    parse_mode="HTML"
                )

        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    text=M_TITLE[3],
                                    parse_mode="HTML")
        await state.finish()


###################### HANDLERS AND CALLBACKS FOR HEROES ####################################################################

@dp.message_handler(text=['Heroes', '🤴🏻 Heroes'])
async def castle_hero(message: types.Message, state: FSMContext):
    list_data = castle_heroes(cursor)
    await bot.send_message(message.from_user.id,
                           text=H_TITLE[0],
                           parse_mode="HTML",
                           reply_markup=inkey_bord_1(list_data))
    await state.set_state(Heroes.choose_castle)


@dp.callback_query_handler(state=Heroes.choose_castle)
async def sex_hero(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == "cancel":
        await state.finish()
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                message_id=callback_query.message.message_id,
            text=INFO_C.format(callback_query.message.from_user), parse_mode="HTML",
        )
        return
    await state.update_data(chosen_caslte=callback_query.data)
    list_data = sex_heroes(cursor)
    user_data = await state.get_data()
    await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                message_id=callback_query.message.message_id,
                                reply_markup=inkey_bord_2(list_data),
                                text=H_TITLE[1].format(user_data['chosen_caslte']),
                                parse_mode="HTML")
    await state.set_state(Heroes.choose_sex)


@dp.callback_query_handler(state=Heroes.choose_sex)
async def rank_hero(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'come_back':
        list_data = castle_heroes(cursor)
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_2(list_data),
                                    text=H_TITLE[0],
                                    parse_mode="HTML")
        await state.set_state(Heroes.choose_castle)
    else:
        await state.update_data(choose_sex=callback_query.data)
        user_data = await state.get_data()
        list_data = rank_heroes(cursor, castle=user_data['chosen_caslte'], sex=user_data['choose_sex'])

        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_2(list_data),
                                    text=H_TITLE[2].format(user_data['chosen_caslte']),
                                    parse_mode="HTML")
        await state.set_state(Heroes.choose_rank)


@dp.callback_query_handler(state=Heroes.choose_rank)
async def heroes_list(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'come_back':
        list_data = sex_heroes(cursor)
        user_data = await state.get_data()
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_2(list_data),
                                    text=H_TITLE[1].format(user_data['chosen_caslte']),
                                    parse_mode="HTML")
        await state.set_state(Heroes.choose_sex)
    else:
        await state.update_data(choose_rank=callback_query.data)
        user_data = await state.get_data()
        upgrade_list = list_heroes(cursor, castle=user_data['chosen_caslte'], sex=user_data['choose_sex'],
                                   heroes_rank=user_data['choose_rank'])
        await state.update_data(all_heroes=upgrade_list[1])
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_2(upgrade_list[0]),
                                    text=H_TITLE[3].format(user_data['chosen_caslte']),
                                    parse_mode="HTML")
        await state.set_state(Heroes.choose_heroes)


@dp.callback_query_handler(state=Heroes.choose_heroes)
async def show_hero(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'come_back':
        list_data = list_heroes(cursor)
        user_data = await state.get_data()
        upgrade_list = list_heroes(cursor, castle=user_data['chosen_caslte'], sex=user_data['choose_sex'],
                                   heroes_rank=user_data['choose_rank'])
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_2(upgrade_list[0]),
                                    text=H_TITLE[2].format(user_data['chosen_caslte']),
                                    parse_mode="HTML")
        await state.set_state(Heroes.choose_rank)
    else:
        user_data = await state.get_data()
        text_msg = 'none'
        for each in user_data['all_heroes']:
            if each[1] == callback_query.data:
                text_msg = H_C.format(each[1], each[2], each[3], each[4], each[5], each[6],
                                      each[8], each[9], each[10], each[12], each[7])
                await bot.send_photo(
                    chat_id=callback_query.message.chat.id,
                    photo=each[11],
                    caption=text_msg,
                    parse_mode="HTML"
                )

        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    text=H_TITLE[4],
                                    parse_mode="HTML")
        await state.finish()


########################## HANDLERS AND CALLBACKS FOR SPELLS ############################################

@dp.message_handler(text=['Spells', '📚 Spells'])
async def create_spells_keyboard(message: types.Message, state: FSMContext):
    list_data = class_spell(cursor)
    await bot.send_message(message.from_user.id,
                           text=S_TITLE[0],
                           parse_mode="HTML",
                           reply_markup=inkey_bord_1(list_data))
    await state.set_state(Spells.choose_class)


@dp.callback_query_handler(state=Spells.choose_class)
async def create_class(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == "cancel":
        await state.finish()
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                message_id=callback_query.message.message_id,
            text=INFO_C.format(callback_query.message.from_user), parse_mode="HTML",
        )
        return
    await state.update_data(choose_class=callback_query.data)
    list_data = element(cursor)
    user_data = await state.get_data()
    await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                message_id=callback_query.message.message_id,
                                reply_markup=inkey_bord_2(list_data),
                                text=S_TITLE[1].format(user_data["choose_class"]),
                                parse_mode="HTML")
    await state.set_state(Spells.choose_element)


@dp.callback_query_handler(state=Spells.choose_element)
async def create_element(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'come_back':
        list_data = class_spell(cursor)
        user_data = await state.get_data()
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_1(list_data),
                                    text=S_TITLE[0],
                                    parse_mode="HTML")
        await state.set_state(Spells.choose_class)
    else:
        await state.update_data(choose_element=callback_query.data)
        user_data = await state.get_data()
        upgrade_list = list_spells(cursor, class_spell=user_data['choose_class'], element=user_data['choose_element'])
        await state.update_data(all_spells=upgrade_list[1])
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_2(upgrade_list[0]),
                                    text=S_TITLE[2].format(user_data["choose_class"], user_data["choose_element"]),
                                    parse_mode="HTML")
        await state.set_state(Spells.choose_spells)


@dp.callback_query_handler(state=Spells.choose_spells)
async def show_spells(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'come_back':
        list_data = element(cursor)
        user_data = await state.get_data()
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_2(list_data),
                                    text=S_TITLE[1].format(user_data["choose_class"]),
                                    parse_mode="HTML")
        await state.set_state(Spells.choose_element)
    else:
        user_data = await state.get_data()
        text_msg = 'none'
        for each in user_data['all_spells']:
            if each[1] == callback_query.data:
                text_msg = S_C.format(each[1], each[2], each[6], each[7], each[8], each[9],
                                      each[3], each[4], each[5])
                await bot.send_photo(
                    chat_id=callback_query.message.chat.id,
                    photo=each[10],
                    caption=text_msg,
                    parse_mode="HTML"
                )

        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    text=S_TITLE[3],
                                    parse_mode="HTML")
        await state.finish()


########################## HANDLERS AND CALLBACKS FOR ARTIFACTS ############################################

@dp.message_handler(text=['Artifacts', '🏺 Artifacts'])
async def create_art_keyboard(message: types.Message, state: FSMContext):
    list_data = slot(cursor)
    await bot.send_message(message.from_user.id,
                           text=A_TITLE[0],
                           parse_mode="HTML",
                           reply_markup=inkey_bord_1(list_data))
    await state.set_state(Artifacts.choose_slot)


@dp.callback_query_handler(state=Artifacts.choose_slot)
async def create_slot(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == "cancel":
        await state.finish()
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                message_id=callback_query.message.message_id,
            text=INFO_C.format(callback_query.message.from_user), parse_mode="HTML",
        )
        return
    await state.update_data(choose_slot=callback_query.data)
    user_data = await state.get_data()
    list_data = value(cursor, slot=user_data["choose_slot"])
    await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                message_id=callback_query.message.message_id,
                                reply_markup=inkey_bord_2(list_data),
                                text=A_TITLE[1].format(user_data['choose_slot']),
                                parse_mode="HTML")
    await state.set_state(Artifacts.choose_value)


@dp.callback_query_handler(state=Artifacts.choose_value)
async def create_element(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'come_back':
        list_data = slot(cursor)
        user_data = await state.get_data()
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_1(list_data),
                                    text=A_TITLE[0],
                                    parse_mode="HTML")
        await state.set_state(Artifacts.choose_slot)
    else:
        await state.update_data(choose_value=callback_query.data)
        user_data = await state.get_data()
        upgrade_list = list_artifacts(cursor, slot=user_data['choose_slot'], value=user_data['choose_value'])
        await state.update_data(all_artifakts=upgrade_list[1])
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_2(upgrade_list[0]),
                                    text=A_TITLE[2].format(user_data['choose_slot'], user_data['choose_value']),
                                    parse_mode="HTML")
        await state.set_state(Artifacts.choose_artifacts)


@dp.callback_query_handler(state=Artifacts.choose_artifacts)
async def show_spells(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'come_back':
        user_data = await state.get_data()
        list_data = value(cursor, slot=user_data["choose_slot"])
        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    reply_markup=inkey_bord_2(list_data),
                                    text=A_TITLE[1].format(user_data['choose_slot']),
                                    parse_mode="HTML")
        await state.set_state(Artifacts.choose_value)
    else:
        user_data = await state.get_data()
        text_msg = 'none'
        for each in user_data['all_artifakts']:
            if each[1] == callback_query.data:
                text_msg = A_C.format(each[1], each[2], each[8], each[11], each[9], each[3],
                                      each[4], each[5], each[6], each[13], each[10])
                await bot.send_photo(
                    chat_id=callback_query.message.chat.id,
                    photo=each[12],
                    caption=text_msg,
                    parse_mode="HTML"
                )

        await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                    message_id=callback_query.message.message_id,
                                    text=A_TITLE[3],
                                    parse_mode="HTML")
        await state.finish()


if __name__ == "__main__":
    executor.start_polling(dp, on_startup=on_startup, skip_updates=True)

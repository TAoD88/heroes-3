
# Get castle_heroes
def castle_heroes(cursor):
    cursor.execute('''SELECT DISTINCT castle FROM heroes''')
    castles = cursor.fetchall()
    # print([i[0] for i in castles])
    return [i[0] for i in castles]

# Get SEX
def sex_heroes(cursor):
    cursor.execute('''SELECT DISTINCT sex FROM heroes''')
    sex = cursor.fetchall()
    # print([i[0] for i in sex])
    return [i[0] for i in sex]
# Get Rank
def rank_heroes(cursor,castle = "Замок",sex="женский"):
    cursor.execute('''SELECT DISTINCT heroes_rank FROM heroes WHERE castle ='{}' AND sex='{}' '''.format(castle, sex))
    heroes_rank = cursor.fetchall()
    # print([i[0] for i in heroes_rank])
    return [i[0] for i in heroes_rank]

# Get HEROES
def list_heroes(cursor, castle = "Замок", sex="женский", heroes_rank ='священник'):
    cursor.execute('''SELECT * FROM heroes WHERE castle ='{}' AND sex='{}' AND heroes_rank ='{}' '''.format(castle, sex, heroes_rank))
    selected_heroes = cursor.fetchall()
    list_hero = []
    list_info_heroes = []
    for each in selected_heroes:
        list_hero.append(each[1])
        list_info_heroes.append(each)
    return list_hero, list_info_heroes


# Get Baff
def class_spell(cursor):
    cursor.execute('''SELECT DISTINCT class_spell FROM spells''')
    spells = cursor.fetchall()
    return [i[0] for i in spells]

# Get element
def element(cursor):
    cursor.execute('''SELECT DISTINCT element FROM spells''')
    element = cursor.fetchall()
    return [i[0] for i in element]


# Get SPELLS
def list_spells(cursor, class_spell="бафф", element="огонь"):
    cursor.execute('''SELECT * FROM spells WHERE class_spell ='{}' AND element='{}' '''.format(class_spell, element))
    spells = cursor.fetchall()
    list_spells = []
    list_info_spells = []
    for lev in spells:
        list_spells.append(lev[1])
        list_info_spells.append(lev)
    return list_spells, list_info_spells
